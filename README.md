# Test automation of Mobile app, uses Kotlin, Appium, Cucumber on AWS Device farm

This project test the Skedulo app. 
The tests are written in Kotlin, and uses Appium, Cucumber and testNG. 
It can be run on your local laptop or on AWS Device farm.
When running the tests on you local laptop you should have an emulator or a real device connected.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

The project requires that you have the following installed:

```
Maven, node, java, Appium, Android studio
if you don't have those installed follow the example in this link:
https://techsouljours.blogspot.com/2018/08/install-appium-on-mac.html
 
```

### Installing



Clone the project

```
git clone git@bitbucket.org:bjorn_skedulo/skedulo-mobile-test-automation.git
cd skedulo-mobile-test-automation
```

The project is a very basic test that will login a user on the app and verify that the user is logged in.

## Running the tests

You can run the tests on you local environment and in AWS Device farm

### Run tests on you local environment

Before you run tests on your local start an emulator or connect a device to your laptop
###Maven
Use maven to trigger the tests

```
mvn clean package
```
To specify OS (Android is default) and Mobile device, this can also be specified in the pom.xml
```
mvn clean package  -Dplatform=iOS -Ddevicename='Skedulo iPhone X'

```
### To run on AWS device farm

In order to run on AWS device farm you have to create a zip file to upload to AWS

```
mvn clean package -DskipTests
```
###Gradle

Use gradle to trigger the tests on your local

```
tbd
```

## Deployment

Tbd

## Built With

* [Appium](http://www.http://appium.io/) - The mobile automation tool
* [Cucumber](https://cucumber.io/) - The bdd test automation tool
* [Maven](https://maven.apache.org/) - Dependency Management
* [Kotlin](https://kotlinlang.org/) - Programming language


## Contributing


## Versioning


## Authors


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

