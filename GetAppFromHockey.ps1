﻿Param(
     [string]$HockeyAppApiToken,
     [string]$Platform
 )
Add-Type -AssemblyName System.Web.Extensions
$JS = New-Object System.Web.Script.Serialization.JavaScriptSerializer

$avToken = "94c9d811f8734787e3654337542834919914ae2e"
$baseUri = "https://rink.hockeyapp.net/"
$appUri = "api/2/apps"
$appVersionUri = "/app_versions?include_build_urls=true"

$auth = YmFuZGVyc3NvbkBza2VkdWxvLmNvbTpTMml0NEprMGxiMk0=
$head = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
$head = @{}
$head.Add("Authorization","Basic $auth")
$head.Add("X-HockeyAppToken",$HockeyAppApiToken)

#Get app from hockeyapp
$getappIdentifier = $baseUri + $appUri
$getApps_response = Invoke-RestMethod -Method GET -Uri $getappIdentifier -Headers $head

#Get necessary values from response
$appsList = $getApps_response.items;
foreach($app in $getApps_response.apps) {
    if($app.title -eq 'Skedulo' -AND $app.platform -eq $Platform) {
        $appIdentifier = $app.public_identifier
        $bundle_identifier = $app.bundle_identifier
        Write-Host "$appIdentifier : " $appIdentifier
        break
    }
}

#Get app versions from hockeyapp
$getappVersionsUri = $baseUri + $appUri + "/" + $appIdentifier + $appVersionUri
$getAppsVersion_response = Invoke-RestMethod -Method GET -Uri $getappVersionsUri -Headers $head

#Get necessary values from response
$appVersionNumber = $getAppsVersion_response[0].app_versions[0].id
Write-Host " $appVersionNumber : " $appVersionNumber

$format = "apk"
if( $Platform -eq "iOS") {
    $format = "ipa"
}
#Download app from hockeyapp
$downloadUrl = $baseUri + $appUri + "/" + $appIdentifier + "/app_versions/" + $appVersionNumber + "?format=" +$format + "&amp;avtoken=" + $avToken + "&amp;download_origin=hockeyapp"

$scriptDir = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
Write-Output "Script dir :" $scriptDir
$output = $scriptDir + "\" + $bundle_identifier + "." + $format
Write-Output "output : " $output
$start_time = Get-Date

Invoke-WebRequest -Method GET -Uri $downloadUrl -Headers $head -OutFile $output
Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"


Get-ChildItem -Path $scriptDir

