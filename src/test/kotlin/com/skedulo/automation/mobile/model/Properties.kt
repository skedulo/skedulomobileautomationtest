package com.skedulo.automation.mobile.model


data class Properties(
    val AWS: String,
    val mobile_android_app_activity: String,
    val mobile_android_app_name: String,
    val mobile_bundle_id: String,
    val mobile_device_os: String,
    val mobile_devices: List<MobileDevice>,
    val mobile_ios_app_name: String,
    val mobile_app_path: String,
    val mobile_ios_xcodeOrgId: String,
    val mobile_ios_xcodeSigningId: String,
    val test_users: List<TestUser>
)

data class TestUser(
    val user_email: String,
    val user_name: String,
    val user_password: String
)

data class MobileDevice(
    val deviceName: String,
    val platformName: String,
    val platformVersion: String,
    val udid: String
)
