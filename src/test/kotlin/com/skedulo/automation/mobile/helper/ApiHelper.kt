package com.skedulo.automation.mobile.helper

import com.skedulo.automation.mobile.api.ApiClient
import com.skedulo.automation.mobile.api.Region
import com.skedulo.automation.mobile.api.model.Jobs
import com.skedulo.automation.mobile.api.model.Regions
import com.skedulo.automation.mobile.properties.ConfigFileReader
import com.skedulo.automation.mobile.tests.AbstractBaseTests.TestBase.Companion.encrypt
import okhttp3.MediaType
import okhttp3.RequestBody


class ApiHelper {

    private var apiClient: ApiClient = ApiClient(
        encrypt(ConfigFileReader().getTestDataApiUser()),
        encrypt(ConfigFileReader().getTestDataApiPassword()),
        ConfigFileReader().getTestDataApiToken())

    fun getRegions(): Regions.RegionsResponseObject {

        val json = ConfigFileReader().getTestDataApiGetRegionQuery()
        val requestBody: RequestBody = RequestBody.create(MediaType.parse("application/json"), json.toString())
        val call = apiClient.graphQLApi.getRegions(requestBody)
        val response = call.execute()
        println("response code = ${response.code()}")
        if(response.isSuccessful) {
            val data = response.body()
            println(data)
        }
        return response.body()!!
    }
    fun getQueuedJobs(): Jobs.JobsResponseObject {
        val json = ConfigFileReader().getTestDataApiGetQueuedJobsQuery()
        val requestBody: RequestBody = RequestBody.create(MediaType.parse("application/json"), json.toString())
        val jobs = apiClient.graphQLApi.getQueuedJobs(requestBody).execute().body()
        return jobs!!
    }
}
