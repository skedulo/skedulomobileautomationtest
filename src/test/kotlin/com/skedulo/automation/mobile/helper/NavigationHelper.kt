package com.skedulo.automation.mobile.helper

import com.skedulo.automation.mobile.pages.*
import com.skedulo.automation.mobile.tests.AbstractBaseTests.TestBase
import org.testng.Assert

class NavigationHelper {
    fun openMoreMenu(){
        var navigationBarComponent = NavigationBarComponent(TestBase.getAppiumDriver())
        navigationBarComponent.isPageLoaded
        navigationBarComponent.clickMore()
    }

    fun openSettingsPageFromAgenda(){
        openMoreMenu()
        var moreMenu = MorePage(TestBase.getAppiumDriver())
        moreMenu.isPageLoaded
        moreMenu.clickSettings()
        val settingsPage = SettingsPage(TestBase.getAppiumDriver())
        settingsPage.isPageLoaded
    }

    fun openLocalisationFromMoreMenu(){
        var moreMenu = MorePage(TestBase.getAppiumDriver())
        moreMenu.isPageLoaded
        moreMenu.clickSettings()
        val settingsPage = SettingsPage(TestBase.getAppiumDriver())
        settingsPage.isPageLoaded
        settingsPage.clickLocalisation()
        val localisationPage= LocalisationPage(TestBase.getAppiumDriver())
        localisationPage.isPageLoaded
    }
    fun openLocalisationFromAgenda(){
        var homePage = HomePage(TestBase.getAppiumDriver())
        homePage.isPageLoaded
        homePage.clickMoreMenu()
        var moreMenu = MorePage(TestBase.getAppiumDriver())
        moreMenu.isPageLoaded
        moreMenu.clickSettings()
        val settingsPage = SettingsPage(TestBase.getAppiumDriver())
        settingsPage.isPageLoaded
        settingsPage.clickLocalisation()
        val localisationPage= LocalisationPage(TestBase.getAppiumDriver())
        localisationPage.isPageLoaded
    }

    /**
     * This function will navigate through NavigationBarComponent and click back on all pages
     */
    fun navigateAndVerifyhNavigationMenu(){
        var homePage = HomePage(TestBase.getAppiumDriver())
        homePage?.isPageLoaded
        //Offers
        homePage?.clickOffersMenu()
        val offersPage = OffersPage(TestBase.getAppiumDriver())
        Assert.assertTrue(offersPage.isPageLoaded!!, "Offers page is not displayed")
        offersPage.clickBack()
        //Messages
        homePage = HomePage(TestBase.getAppiumDriver())
        homePage?.clickMessagesMenu()
        val messagesPage = MessagesPage(TestBase.getAppiumDriver())
        Assert.assertTrue(messagesPage.isPageLoaded!!, "Messages page is not displayed")
        messagesPage.clickBack()
        //Notifications
        homePage = HomePage(TestBase.getAppiumDriver())
        homePage?.clickNotificationsMenu()
        val notificationsPage = NotificationsPage(TestBase.getAppiumDriver())
        Assert.assertTrue(notificationsPage.isPageLoaded!!, "Notifications page is not displayed")
        notificationsPage.clickBack()
        homePage = HomePage(TestBase.getAppiumDriver())
        Assert.assertTrue(homePage.isPageLoaded!!,"Agenda page is not displayed")
    }

    /**
     * This function will navigate through the menu with the NavigationBarComponent
     */
    fun navigateAndVerifyNavigationMenuAlternative(){
        var homePage = HomePage(TestBase.getAppiumDriver())
        homePage?.isPageLoaded
        //Offers
        homePage?.clickOffersMenu()
        val offersPage = OffersPage(TestBase.getAppiumDriver())
        Assert.assertTrue(offersPage.isPageLoaded!!, "Offers page is not displayed")
        offersPage.getNavigationBarComponent()?.clickMessages()
        //Messages
        val messagesPage = MessagesPage(TestBase.getAppiumDriver())
        Assert.assertTrue(messagesPage.isPageLoaded!!, "Messages page is not displayed")
        messagesPage.getNavigationBarComponent()?.clickNotifications()
        //Notifications
        val notificationsPage = NotificationsPage(TestBase.getAppiumDriver())
        Assert.assertTrue(notificationsPage.isPageLoaded!!, "Notifications page is not displayed")
        notificationsPage.getNavigationBarComponent()?.clickAgenda()
        homePage = HomePage(TestBase.getAppiumDriver())
        Assert.assertTrue(homePage.isPageLoaded!!,"Agenda page is not displayed")
    }
    fun navigateAndVerifyMoreMenu(){
        var moreMenu = MorePage(TestBase.getAppiumDriver())
        moreMenu.isPageLoaded
        moreMenu.clickSettings()
        val settingsPage = SettingsPage(TestBase.getAppiumDriver())
        //Thread.sleep(2000)
        settingsPage.isPageLoaded
        settingsPage.clickBack()
        moreMenu = MorePage(TestBase.getAppiumDriver())
        moreMenu.isPageLoaded
        moreMenu.clickUnavailability()
        val availabilityPage = AvailabilityPage(TestBase.getAppiumDriver())
        Assert.assertTrue(availabilityPage.isPageLoaded!!, "Availability page is not loaded")
        availabilityPage.clickBack()
        moreMenu = MorePage(TestBase.getAppiumDriver())
        moreMenu.clickShifts()
        val shiftsPage = ShiftsPage(TestBase.getAppiumDriver())
        Assert.assertTrue(shiftsPage.isPageLoaded!!, "Shiftspage is not loaded")
        shiftsPage.clickBack()
        val socialFeedPage = SocialFeedPage(TestBase.getAppiumDriver())
        Assert.assertTrue(socialFeedPage.isPageLoaded!!, "Social feed is not loaded")
        socialFeedPage.clickMore()
        moreMenu = MorePage(TestBase.getAppiumDriver())
        moreMenu.isPageLoaded
        moreMenu.navigationBarComponent?.clickAgenda()
        val homePage = HomePage(TestBase.getAppiumDriver())
        homePage?.isPageLoadedLong
    }
}
