package com.skedulo.automation.mobile.helper

import com.skedulo.automation.mobile.model.Properties
import com.skedulo.automation.mobile.pages.LoginPage
import com.skedulo.automation.mobile.pages.LoginTeamNamePage
import com.skedulo.automation.mobile.pages.MoreOptionsPage
import com.skedulo.automation.mobile.pages.SalesForceLoginPage
import com.skedulo.automation.mobile.tests.AbstractBaseTests.TestBase
import com.skedulo.automation.mobile.tests.AbstractBaseTests.TestBase.Companion.getAppiumDriver
import com.skedulo.automation.mobile.tests.AbstractBaseTests.TestBase.Companion.os
import com.skedulo.automation.mobile.util.OS

class LoginHelper {

    fun login(properties: Properties, index: Int) {
        //Get default values for login
        val email = properties.test_users.get(index).user_email
        val password = properties.test_users.get(index).user_password
        login(email, password)
    }
    fun login() {
        //Get default values for login
        val email = "xtest1@skedulo.com"
        val password = "54321login"
        login(email, password)
    }
    // The below is added by Jia
    fun loginWithDifferenUser() {
        //Get default values for login
        val email = "xtest2@skedulo.com"
        val password = "54321login"
        login(email, password)
    }
    fun login(email: String, password: String) {
        val loginPage = LoginPage(TestBase.getAppiumDriver())
        loginPage.isPageLoaded
        loginPage.clickSignInWithSalesForce()
        val sfLoginPage = SalesForceLoginPage(TestBase.getAppiumDriver())
        sfLoginPage.isPageLoaded
        sfLoginPage.setEmail(email)
        sfLoginPage.setPassword(password)
        try {
            TestBase.getAppiumDriver().hideKeyboard()
        } catch(e: Exception) {} //No keyboard was present
        sfLoginPage.clickLogin()

    }
    fun temporaryFixForLogin(){
        val moreOptionsPage = MoreOptionsPage(TestBase.getAppiumDriver())
        moreOptionsPage.isPageLoaded
        moreOptionsPage.clickLogInToAnotherTeam()

        val loginPage = LoginPage(TestBase.getAppiumDriver())
        loginPage.isPageLoaded
        loginPage.clickSignInWithSalesForce()

    }
    fun loginStandalone(){
        val loginPage = LoginPage(getAppiumDriver())
        loginPage.isPageLoaded
        loginPage.clickSignInWithTeamName()
        val loginTeamNamePage = LoginTeamNamePage(getAppiumDriver())
        loginTeamNamePage.isPageLoaded
        loginTeamNamePage.setTeamName("btest")

        loginTeamNamePage.clickNext()
    }
    fun temporaryFixForStandaloneLogin(){
        val moreOptionsPage = MoreOptionsPage(TestBase.getAppiumDriver())
        moreOptionsPage.isPageLoaded
        moreOptionsPage.clickLogInToAnotherTeam()
        loginStandalone()
    }

}
