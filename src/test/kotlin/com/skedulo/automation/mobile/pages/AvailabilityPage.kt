package com.skedulo.automation.mobile.pages

import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSBy
import io.appium.java_client.pagefactory.iOSXCUITFindBy
import org.openqa.selenium.By
import org.openqa.selenium.support.PageFactory

class AvailabilityPage (driver: AppiumDriver<*>) : BasePage(driver) {

    @iOSXCUITFindBy(accessibility = "Availability")
    @AndroidFindBy(xpath = "//*[@text='Availability']")
    private val title: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Unavailability")
    @AndroidFindBy(xpath = "//android.widget.ImageButton[contains(@content-desc,‘Navigate up’)]")
    private val backBtn: MobileElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }
    val isPageLoaded: Boolean?
        get() = title!!.isDisplayed

    fun clickBackBtn(){
        backBtn!!.click()
    }
}
