package com.skedulo.automation.mobile.pages

import com.skedulo.automation.mobile.tests.AbstractBaseTests.TestBase
import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSBy
import io.appium.java_client.pagefactory.iOSXCUITFindBy
import org.openqa.selenium.By
import org.openqa.selenium.support.PageFactory

class MessagesPage(driver: AppiumDriver<*>) : BasePage(driver) {
    private var navigationBarComponent : NavigationBarComponent? = null

    @iOSXCUITFindBy(accessibility = "Messages")
    @AndroidFindBy(xpath = "//*[@text='Messages']")
    private val title: MobileElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
        navigationBarComponent = NavigationBarComponent(TestBase.getAppiumDriver())
    }
    val isPageLoaded: Boolean?
        get() {
            return title!!.isDisplayed
        }
    fun getNavigationBarComponent(): NavigationBarComponent? {
        return navigationBarComponent
    }
}
