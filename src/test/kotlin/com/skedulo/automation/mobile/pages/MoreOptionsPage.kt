package com.skedulo.automation.mobile.pages

import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSXCUITFindBy
import org.openqa.selenium.support.PageFactory

class MoreOptionsPage(driver: AppiumDriver<*>) : BasePage(driver) {

    @iOSXCUITFindBy(accessibility = "More options")
    @AndroidFindBy(xpath = "//*[@text='More options']")
    private val title: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "ui_DiffTeam_title")
    @AndroidFindBy(xpath = "//*[@text='Log in to another team…']")
    private val logIntoAndotherTeamBtn: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Back")
    @AndroidFindBy(xpath = "//*[@text='Back']")
    private val backBtn: MobileElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }
    val isPageLoaded: Boolean?
        get() {
            return title!!.isDisplayed
        }
    fun clickLogInToAnotherTeam(){
        waitForElement(logIntoAndotherTeamBtn, 10)
        logIntoAndotherTeamBtn!!.click()
    }
}
