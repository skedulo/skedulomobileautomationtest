package com.skedulo.automation.mobile.pages

import com.skedulo.automation.mobile.util.OS
import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSXCUITFindBy
import org.openqa.selenium.By
import org.openqa.selenium.support.PageFactory

class StandaloneLoginPage(driver: AppiumDriver<*>) : BasePage(driver) {

    //@iOSXCUITFindBy(className = "XCUIElementTypeTextField")
    @iOSXCUITFindBy(accessibility = "yours@example.com")
    @AndroidFindBy(xpath = "//*[@class='android.widget.EditText' and ./parent::*[./parent::*[(./preceding-sibling::* | ./following-sibling::*)[@class='android.view.View']]]]")
    private val emailField: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "yours@example.com")
    @AndroidFindBy(xpath = "//*[@name='email']")
    private val altEmailField: MobileElement? = null

    //@iOSXCUITFindBy(className = "XCUIElementTypeSecureTextField")
    @iOSXCUITFindBy(accessibility = "your password")
    @AndroidFindBy(xpath = "//*[@class='android.widget.EditText' and ./parent::*[./parent::*[./parent::*[(./preceding-sibling::* | ./following-sibling::*)[@class='android.view.View']]]]]")
    private val passField: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "your password")
    @AndroidFindBy(xpath = "//*[@name='password']")
    private val altPassField: MobileElement? = null


    //@iOSXCUITFindBy(className = "XCUIElementTypeButton")
    @iOSXCUITFindBy(accessibility = "Log In")
    @AndroidFindBy(xpath = "//*[@text='Log In']")
    private val loginBtn: MobileElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }

    val isPageLoaded: Boolean?
        get() {
            waitForElement(emailField, 10)
            return emailField?.isDisplayed
        }
    fun setEmail(email: String) {
        waitForElement(emailField, 10)
        if(os == OS.IOS || os == OS.AWSIOS) {
            emailField?.click()
        }
        try {
            emailField!!.setValue(email)
        }catch(e: java.lang.Exception){
            altEmailField!!.click()
            altEmailField!!.setValue(email)
        }

    }
    fun setPassword(password: String) {
        passField!!.click()
        if (passField.text != password) {
            try {
                passField.setValue(password)
            }catch(e: java.lang.Exception){
                altPassField!!.click()
                altPassField!!.setValue(password)
            }
        }
    }
    fun clickLogin() {
        if(os == OS.ANDROID || os == OS.AWSANDROID) {
            loginBtn!!.click()
        }

    }
}
