package com.skedulo.automation.mobile.pages

import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSXCUITFindBy
import org.openqa.selenium.support.PageFactory

class SocialFeedPage(driver: AppiumDriver<*>) : BasePage(driver) {

    @iOSXCUITFindBy(accessibility = "Social Feed")
    @AndroidFindBy(xpath = "//*[@text='Social Feed']")
    private val title: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Send message")
    @AndroidFindBy(xpath = "//*[@text='Send message']")
    private val sendMessageBtn: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Done")
    @AndroidFindBy(xpath = "//*[@text='Done']")
    private val doneBtn: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "ChatterNewMessage")
    @AndroidFindBy(xpath = "//*[@text='Send message']")
    private val meessageField: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Send message")
    @AndroidFindBy(xpath = "//*[@text='Send message']")
    private val sendMessageArrow: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "More")
    @AndroidFindBy(xpath = "//*[@text='More']")
    private val moreArrow: MobileElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }
    val isPageLoaded: Boolean?
        get() {
            return title!!.isDisplayed
        }
    fun clickSendMessage(){
        sendMessageBtn?.click()
    }
    fun clickDone(){
        doneBtn?.click()
    }
    fun clickMore(){
        moreArrow?.click()
    }
}
