package com.skedulo.automation.mobile.pages

import com.skedulo.automation.mobile.tests.AbstractBaseTests.TestBase
import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSXCUITFindBy
import org.openqa.selenium.support.PageFactory

class LoginTeamNamePage(driver: AppiumDriver<*>) : BasePage(driver) {

    @iOSXCUITFindBy(accessibility = "Log in")
    @AndroidFindBy(xpath = "//*[@text='Log in']")
    private val title: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "TextTeamName")
    @AndroidFindBy(xpath = "//*[@text='Enter team name']")
    private val enterTeamName: MobileElement? = null

    @iOSXCUITFindBy(id = "Next")
    @AndroidFindBy(xpath = "//*[@text='Next']")
    private val nextBtn: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Back")
    @AndroidFindBy(xpath = "//*[@class='android.widget.ImageButton']")
    private val backArrow: MobileElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }
    val isPageLoaded: Boolean?
        get() {
            return title!!.isDisplayed
        }
    fun setTeamName(teamName: String){
        enterTeamName!!.sendKeys(teamName)
    }
    fun clickNext() {
        nextBtn!!.click()
    }
    fun clickBackArrow() {
        backArrow!!.click()
    }
}
