package com.skedulo.automation.mobile.pages

import com.skedulo.automation.mobile.tests.AbstractBaseTests.TestBase
import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.*
import org.openqa.selenium.support.PageFactory

class MorePage (driver: AppiumDriver<*>) : BasePage(driver) {
    var navigationBarComponent : NavigationBarComponent? = null

    @iOSXCUITFindBy(accessibility = "More")
    @AndroidFindBy(xpath = "//*[@text='More']")
    private val title: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Settings")
    @AndroidFindBy(xpath = "//*[@text=\"Settings\"]")
    private val SettingsBtn: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Availability")
    @AndroidFindBy(xpath = "//*[@text=\"Availability\"]")
    private val UnavailabiltyBtn: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Shifts")
    @AndroidFindBy(xpath = "//*[@text=\"Shifts\"]")
    private val ShiftsBtn: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Feedback and Support")
    @AndroidFindBy(xpath = "//*[@text=\"Feedback and Support\"]")
    private val FeedbackBtn: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Log Out")
    @AndroidFindBy(xpath = "//*[@text=\"Log Out\"]")
    private val LogOutBtn: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Social Feed")
    @AndroidFindBy(xpath = "//*[@text=\"Social Feed\"]")
    private val SyncBtn : MobileElement? = null

    @iOSXCUITFindBy(iOSClassChain= "**/XCUIElementTypeCell//XCUIElementTypeImage")//**/XCUIElementTypeCell[`visible == 1`][$type == "XCUIElementTypeImage"
    @AndroidFindBy(id = "moreProfileCellAvatar")
    private val ResourceDetailsBtn: MobileElement? = null

    @iOSXCUITFindBy(xpath= "//XCUIElementTypeImage")
    @AndroidFindBy(id = "moreProfileCellAvatar")
    private val ResourceDetailsImage: MobileElement? = null

    @iOSXCUITFindBy(className= "XCUIElementTypeStaticText")
    @AndroidFindBy(id = "moreProfileCellName")
    private val ResourceName: MobileElement? = null

    @iOSXCUITFindBy(accessibility= "xtest")
    @AndroidFindBy(id = "moreProfileCellTeamName")
    private val ResourceTeamName: MobileElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
        navigationBarComponent = NavigationBarComponent(TestBase.getAppiumDriver())
    }
    val isPageLoaded: Boolean
        get() = title!!.isDisplayed

    fun clickSettings() {
        SettingsBtn!!.click()
    }
    fun clickUnavailability() {
        UnavailabiltyBtn!!.click()
    }
    fun clickShifts() {
        ShiftsBtn!!.click()
    }
    fun clickFeedBack() {
        FeedbackBtn!!.click()
    }
    fun clickLogout() {
        LogOutBtn!!.click()
    }
    fun clickSync() {
        SyncBtn!!.click()
    }
    fun clickResource() {
        waitForElement(ResourceTeamName)
        ResourceTeamName?.click()
        //ResourceDetailsBtn?.click()

    }
}
