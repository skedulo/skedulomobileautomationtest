package com.skedulo.automation.mobile.pages

import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSXCUITFindBy
import org.openqa.selenium.support.PageFactory

class NavigationBarComponent(driver: AppiumDriver<*>) : BasePage(driver) {
    @iOSXCUITFindBy(accessibility = "Agenda")
    @AndroidFindBy(id="navigation_bar_tab_1")
    //@AndroidFindBy(xpath = "//*[@text=\"Agenda\"]")
    private val Agenda: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Offers")
    @AndroidFindBy(id="navigation_bar_tab_2")
    //@AndroidFindBy(xpath = "//*[@text=\"Offers\"]")
    private val Offers: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Messages")
    @AndroidFindBy(id="navigation_bar_tab_3")
    //@AndroidFindBy(xpath = "//*[@text=\"Chat\"]")
    private val Messages: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Notifications")
    @AndroidFindBy(id="navigation_bar_tab_4")
    //@AndroidFindBy(xpath = "//*[@text=\"Notifications\"]")
    private val Notifications: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "More")
    @AndroidFindBy(id="navigation_bar_tab_5")
    //@AndroidFindBy(xpath = "//*[@text=\"More\"]")
    private val More: MobileElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }
    val isPageLoaded: Boolean?
        get() {
            waitForElement(Agenda, 5)
            return Agenda!!.isDisplayed
        }
    fun clickAgenda() {
        Agenda!!.click()
    }
    fun clickOffers() {
        Offers!!.click()
    }
    fun clickMessages() {
        Messages!!.click()
    }
    fun clickNotifications() {
        Notifications!!.click()
    }
    fun clickMore() {
        More!!.click()
    }
}
