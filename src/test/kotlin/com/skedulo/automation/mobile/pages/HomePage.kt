package com.skedulo.automation.mobile.pages


import com.skedulo.automation.mobile.tests.AbstractBaseTests.TestBase
import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSBy
import io.appium.java_client.pagefactory.iOSXCUITFindBy
import org.openqa.selenium.support.PageFactory


class HomePage(driver: AppiumDriver<*>) : BasePage(driver) {
    private var navigationBarComponent : NavigationBarComponent? = null

    @iOSXCUITFindBy(accessibility = "Agenda")
    @AndroidFindBy(xpath = "//*[@text=\"Agenda\"]")
    private val title: MobileElement? = null

    @iOSBy(id = "TODAY")
    @AndroidFindBy(xpath = "//*[@text=\"TODAY\"]")
    private val TodayBtn: MobileElement? = null

    @iOSBy(id = "WEEK")
    @AndroidFindBy(xpath = "//*[@text=\"WEEK\"]")
    private val WeekBtn: MobileElement? = null

    @iOSBy(id = "MONTH")
    @AndroidFindBy(xpath = "//*[@text=\"MONTH\"]")
    private val MonthBtn: MobileElement? = null

    @iOSBy(id = "Add")
    @AndroidFindBy(xpath = "//*[@contentDescription=\"Add\"]")
    private val AddBtn: MobileElement? = null

    @iOSBy(accessibility = "")
    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc=\"OK\"]")
    private val MenuBtn: MobileElement? = null

    @iOSBy(id = "LabelUserWelcome")
    @AndroidFindBy(xpath = "android.widget.TextView[@content-desc=\"LabelUserWelcome\"]")
    private val UserWelcome: MobileElement? = null

    @iOSBy(id = "LabelUserAvailability")
    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"LabelUserAvailability\"]")
    private val UserAvailability: MobileElement? = null

    @iOSBy(accessibility = "")
    @AndroidFindBy(xpath = "//*[@text=\"No jobs or activities\"]")
    private val NoJobsOrActivities: MobileElement? = null

    //Navigation
//    @iOSXCUITFindBy(accessibility = "Agenda")
//    @AndroidFindBy(id="navigation_bar_tab_1")
//    //@AndroidFindBy(xpath = "//*[@text=\"Agenda\"]")
//    private val AgendaMenu: MobileElement? = null
//
//    @iOSXCUITFindBy(accessibility = "Offers")
//    @AndroidFindBy(id="navigation_bar_tab_2")
//    //@AndroidFindBy(xpath = "//*[@text=\"Offers\"]")
//    private val OffersMenu: MobileElement? = null
//
//    @iOSXCUITFindBy(accessibility = "Messages")
//    @AndroidFindBy(id="navigation_bar_tab_3")
//    //@AndroidFindBy(xpath = "//*[@text=\"Chat\"]")
//    private val MessagesMenu: MobileElement? = null
//
//    @iOSXCUITFindBy(accessibility = "Notifications")
//    @AndroidFindBy(id="navigation_bar_tab_4")
//    //@AndroidFindBy(xpath = "//*[@text=\"Notifications\"]")
//    private val NotificationsMenu: MobileElement? = null
//
//    @iOSXCUITFindBy(accessibility = "More")
//    @AndroidFindBy(id="navigation_bar_tab_5")
//    //@AndroidFindBy(xpath = "//*[@text=\"More\"]")
//    private val MoreMenu: MobileElement? = null


    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
        navigationBarComponent = NavigationBarComponent(TestBase.getAppiumDriver())
    }
    val isPageLoaded: Boolean?
        get() {
            waitForElement(title, 10)
            return title!!.isDisplayed
        }
    val isPageLoadedLong: Boolean?
        get() {
            waitForElement(title, 30)
            return title!!.isDisplayed
        }
    fun clickMenu() {
        MenuBtn!!.click()
    }
    fun clickAgendaMenu() {
        navigationBarComponent?.clickAgenda()
        //AgendaMenu!!.click()
    }
    fun clickOffersMenu() {
        navigationBarComponent?.clickOffers()
        //OffersMenu!!.click()
    }
    fun clickMessagesMenu() {
        navigationBarComponent?.clickMessages()
        //MessagesMenu!!.click()
    }
    fun clickNotificationsMenu() {
        navigationBarComponent?.clickNotifications()
        //NotificationsMenu!!.click()
    }
    fun clickMoreMenu() {
        navigationBarComponent?.clickMore()
        //MoreMenu!!.click()
    }
    fun getNavigationBarComponent(): NavigationBarComponent? {
        return navigationBarComponent
    }
}

