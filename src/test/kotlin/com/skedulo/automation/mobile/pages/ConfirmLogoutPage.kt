package com.skedulo.automation.mobile.pages

import com.skedulo.automation.mobile.util.OS
import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSBy
import io.appium.java_client.pagefactory.iOSXCUITFindBy
import org.openqa.selenium.support.PageFactory

class ConfirmLogoutPage(driver: AppiumDriver<*>) : BasePage(driver) {


    @iOSXCUITFindBy(iOSClassChain= "**/XCUIElementTypeOther/XCUIElementTypeButton")
    //@iOSXCUITFindBy(accessibility = "Log Out")
    //@iOSXCUITFindBy(xpath = "//*[@text='Log Out' and @class='UIAButton']")
    @AndroidFindBy(id = "button1")
    private val LogOutBtn: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Cancel")
    @AndroidFindBy(id = "button2")
    private val CancelBtn: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Confirm Log Out")
    @AndroidFindBy(xpath = "//*[@text=\"Confirm Log Out\"]")
    private val PageTitle: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Logging out will remove all local data")
    @AndroidFindBy(xpath = "//*[@text=\"Logging out will remove all local data\"]")
    private val PageText: MobileElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }
    val pageTitle: String
        get() = PageTitle!!.text
    val pageText: String
        get() = PageText!!.text
    val isPageLoaded: Boolean
        get() = PageTitle!!.isDisplayed

    fun clickLogout() {
        //waitForElement(LogOutBtn, 5)
        when(os){
            OS.IOS, OS.AWSIOS -> {
                val elements= driver.findElementsByAccessibilityId("Log Out")
                //println("Found nr of elements ${elements.size}")
                elements[elements.size -1].click()
            }
            OS.ANDROID, OS.AWSANDROID -> {
                LogOutBtn?.click()
            }
        }
    }

    fun clickCancel() {
        CancelBtn!!.click()
    }

}
