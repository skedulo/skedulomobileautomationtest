package com.skedulo.automation.mobile.pages

import com.skedulo.automation.mobile.tests.AbstractBaseTests.TestBase
import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSBy
import io.appium.java_client.pagefactory.iOSXCUITFindBy
import org.openqa.selenium.By
import org.openqa.selenium.support.PageFactory

class OffersPage(driver: AppiumDriver<*>) : BasePage(driver) {
    private var navigationBarComponent : NavigationBarComponent? = null
    @iOSXCUITFindBy(accessibility = "Offers")
    @AndroidFindBy(xpath = "//*[@text='Offers']")
    private val title: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "OPEN")
    @AndroidFindBy(xpath = "//*[@text='OPEN']")
    private val OpenOffersBtn: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "SUCCESSFUL")
    @AndroidFindBy(xpath = "//*[@text='SUCCESSFUL']")
    private val SuccessfulOffersBtn: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "EXPIRED")
    @AndroidFindBy(xpath = "//*[@text='EXPIRED']")
    private val ExpiredOffersBtn: MobileElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
        navigationBarComponent = NavigationBarComponent(TestBase.getAppiumDriver())
    }
    val isPageLoaded: Boolean?
        get() {
            return title!!.isDisplayed
        }
    fun clickOpenOffers() {
        OpenOffersBtn!!.click()
    }
    fun clickSuccessfulOffers() {
        SuccessfulOffersBtn!!.click()
    }
    fun clickExpiredOffers() {
        ExpiredOffersBtn!!.click()
    }
    fun getNavigationBarComponent(): NavigationBarComponent? {
        return navigationBarComponent
    }
}
