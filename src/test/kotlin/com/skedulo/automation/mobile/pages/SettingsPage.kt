package com.skedulo.automation.mobile.pages

import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSXCUITFindBy
import org.openqa.selenium.support.PageFactory

class SettingsPage(driver: AppiumDriver<*>) : BasePage(driver)  {
    @iOSXCUITFindBy(accessibility = "Settings")
    @AndroidFindBy(xpath = "//*[@text='Settings']")
    private val title: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Localisation - button")
    @AndroidFindBy(xpath = "//*[@text='Localisation']")
    private val localisation: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Auto refresh job details")
    @AndroidFindBy(xpath = "//*[@text='Auto refresh job details']")
    private val autoRefreshJobDetails: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Skip navigation options")
    @AndroidFindBy(xpath = "//*[@text='Skip navigation options']")
    private val skipNavigationOptions: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Save original photos")
    @AndroidFindBy(xpath = "//*[@text='Save original photos']")
    private val saveOriginalOptions: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Attachment on Wi-Fi only")
    @AndroidFindBy(xpath = "//*[@text='Attachment on Wi-Fi only']")
    private val sttachmentOnWifiOnly: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Enable developer features")
    @AndroidFindBy(xpath = "//*[@text='Enable developer features']")
    private val enableDeveloperFeatures: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "iCalendar link - button")
    @AndroidFindBy(xpath = "//*[@text='iCalendar link']")
    private val iCalendarLink: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Copy link")
    @AndroidFindBy(xpath = "//*[@text='Copy Link']")
    private val copyLink: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Feedback & support")
    @AndroidFindBy(xpath = "//*[@text='Feedback & support']")
    private val feedbackAndSupport: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Unavailability")
    @AndroidFindBy(xpath = "//android.widget.ImageButton[contains(@content-desc,‘Navigate up’)]")
    private val backBtn: MobileElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }
    val isPageLoaded: Boolean?
        get() {
            waitForElement(title, 5)
            return title!!.isDisplayed
        }
    fun clickLocalisation()
    {
        localisation?.click()
    }
    fun clickBackBtn(){
        backBtn!!.click()
    }
}
