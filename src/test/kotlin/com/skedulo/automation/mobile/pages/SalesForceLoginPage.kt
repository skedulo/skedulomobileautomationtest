package com.skedulo.automation.mobile.pages

import com.skedulo.automation.mobile.util.OS
import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSBy
import io.appium.java_client.pagefactory.iOSXCUITFindBy
import org.openqa.selenium.support.PageFactory

class SalesForceLoginPage(driver: AppiumDriver<*>) : BasePage(driver) {


    @iOSXCUITFindBy(className = "XCUIElementTypeTextField")
    @AndroidFindBy(id="username")
    private val emailField: MobileElement? = null

    @iOSBy(id = "username")
    @AndroidFindBy(id = "idcard-identity")
    private val idcard: MobileElement? = null

    @iOSBy(accessibility = "")
    @AndroidFindBy(id = "clear_link")
    private val clearUserName: MobileElement? = null

    @iOSBy(accessibility = "")
    @AndroidFindBy(id = "hint_back_chooser")
    private val savedUserNames: MobileElement? = null

    @iOSXCUITFindBy(className = "XCUIElementTypeSecureTextField")
    @AndroidFindBy(id = "password")
    private val passField: MobileElement? = null

    @AndroidFindBy(id = "theloginform")
    private val theLoginForm: MobileElement? = null

    @iOSXCUITFindBy(id = "Log In")
    @AndroidFindBy(id = "Login")
    private val loginBtn: MobileElement? = null

    @iOSBy(xpath ="//*[@text='Remember me']")
    @AndroidFindBy(id = "rememberUn")
    private val rememberMe: MobileElement? = null

    @iOSBy(xpath = "//*[@text='Done']")
    @AndroidFindBy(id = "close_button")
    private val closeBtn: MobileElement? = null

    @iOSBy(xpath = "//*[@text='Done']")
    @AndroidFindBy(xpath = "//*[@id='logo']")
    private val salesForceLogo: MobileElement? = null

    @iOSBy(id = "")
    @AndroidFindBy(id = "error")
    private val errorText: MobileElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }

    val isPageLoaded: Boolean?
        get() {
            waitForElement(emailField, 10)
            return emailField?.isDisplayed
        }
    fun setEmail(email: String) {
        waitForElement(emailField, 10)
        emailField?.click()
        emailField?.clear()
        emailField?.sendKeys(email)

    }

    fun setPassword(password: String) {
        passField!!.click()
        if (passField.text != password) {
            passField.clear()
            passField.sendKeys(password)
        }
    }

    fun clickLogin() {
        loginBtn!!.click()
    }

    fun clickLoginForm() {
        theLoginForm!!.click()
    }

    fun clickRememberMe() {
        rememberMe!!.click()
    }

    fun clickCloseButton() {
        closeBtn!!.click()
    }

    val isErrorDisplayed: Boolean
        get() = errorText!!.isDisplayed

    // Please check your username and password. If you still can't log in, contact your Salesforce administrator.
    fun getErrorText(): String {
        return errorText!!.text
    }
}
