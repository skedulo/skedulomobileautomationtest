package com.skedulo.automation.mobile.pages


import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSBy
import org.openqa.selenium.support.PageFactory

class MenuPage(driver: AppiumDriver<*>) : BasePage(driver) {

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"Agenda\"]")
    private val AgendaBtn: MobileElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"Unavailability\"]")
    private val UnavailabilityBtn: MobileElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"Social Feed\"]")
    private val SocialFeedBtn: MobileElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"Notifications\"]")
    private val NotificationsBtn: MobileElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"Offers\"]")
    private val OffersBtn: MobileElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"Settings\"]")
    private val SettingsBtn: MobileElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"LogOut\"]")
    private val LogOutBtn: MobileElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }

    val isPageLoaded: Boolean
        get() = AgendaBtn!!.isDisplayed

    fun clickLogOut() {
        LogOutBtn!!.click()
    }

    fun swipeToBottom() {
        swipeByCordinates(442, 512, 442, 10)
    }

}
