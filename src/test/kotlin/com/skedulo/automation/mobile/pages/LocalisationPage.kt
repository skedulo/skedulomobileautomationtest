package com.skedulo.automation.mobile.pages

import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSXCUITFindBy
import org.openqa.selenium.support.PageFactory

class LocalisationPage(driver: AppiumDriver<*>) : BasePage(driver) {

    @iOSXCUITFindBy(accessibility = "Localisation")
    @AndroidFindBy(xpath = "//*[@text='Localisation']")
    private val title: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "LabelKilometres")
    @AndroidFindBy(xpath = "//*[@text='Kilometres']")
    private val KilometresBtn: MobileElement? = null


    @iOSXCUITFindBy(accessibility = "LabelMiles")
    @AndroidFindBy(xpath = "//*[@text='Miles']")
    private val MilesBtn: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "LabelDMY")
    @AndroidFindBy(xpath = "//*[@text='Day/Month/Year']")
    private val DayMonthYearBtn: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "LabelMDY")
    @AndroidFindBy(xpath = "//*[@text='Month/Day/Year']")
    private val MonthDayYearBtn: MobileElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }
    val isPageLoaded: Boolean?
        get() {
            return title!!.isDisplayed
        }

    fun clickKilometres() {
        KilometresBtn!!.click()
    }
    fun clickMiles() {
        MilesBtn!!.click()
    }
    fun clickDayMonthYear() {
        DayMonthYearBtn!!.click()
    }
    fun clickMonthDayYear() {
        MonthDayYearBtn!!.click()
    }

    val isTickOnKilometres: Boolean?
        get() {
            //NOTE: The tick is an image and there's no way of knowing if it's ticked or not.
            return KilometresBtn!!.isEnabled
        }

    val isTickOnMiles: Boolean?
        get() {
            return MilesBtn!!.isEnabled
        }
    val isTickOnDayMonthYear: Boolean?
        get() {
            return DayMonthYearBtn!!.isEnabled
            //return DayMonthYearBtn!!.isSelected
        }
    val isTickOnMonthDayYear: Boolean?
        get() {
            return MonthDayYearBtn!!.isEnabled
            //return MonthDayYearBtn!!.isSelected
        }
}

