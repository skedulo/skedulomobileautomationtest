package com.skedulo.automation.mobile.pages

import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSXCUITFindBy
import org.openqa.selenium.support.PageFactory
import java.lang.Exception

class EditProfilePage (driver: AppiumDriver<*>) : BasePage(driver) {

    @iOSXCUITFindBy(id = "Edit profile")
    @AndroidFindBy(xpath = "//*[@text='Edit profile']")
    private val title: MobileElement? = null

    @iOSXCUITFindBy(id = "ui_ProfileContainer_title")
    @AndroidFindBy(accessibility = "ui_ProfileContainer_title")
    private val resourceName: MobileElement? = null

    @iOSXCUITFindBy(id = "ui_ProfileEmail_value")
    @AndroidFindBy(accessibility = "ui_ProfileEmail_title")
    private val resourceEmail: MobileElement? = null

    @iOSXCUITFindBy(id = "ui_ProfilePhone_countrycode")
    @AndroidFindBy(accessibility = "ui_ProfilePhone_countrycode")
    private val resourceContryCode: MobileElement? = null

    @iOSXCUITFindBy(id = "ui_ProfilePhone_phone")
    @AndroidFindBy(accessibility = "ui_ProfilePhone_phone")
    private val resourceMobileNumber: MobileElement? = null

    @iOSXCUITFindBy(id = "ui_ProfileAddress_value")
    @AndroidFindBy(accessibility = "ui_ProfileAddress_value")
    private val resourceAddress: MobileElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }
    val isPageLoaded: Boolean?
    get() {
        var result: Boolean = false
        try {
            if(title!!.isDisplayed){result = true}
        }catch(e:Exception){
            takeScreenshot("EditProfilePage.isPageLoaded ")
        }
        return result
    }

    fun getResourceName():String {
        return resourceName!!.text
    }

    fun getResourceEmail():String {
        return resourceEmail!!.text
    }

    fun getResourceMobileNumber():String {
        return resourceMobileNumber!!.text
    }

    fun getResourceAddress():String {
        return resourceAddress!!.text
    }

}
