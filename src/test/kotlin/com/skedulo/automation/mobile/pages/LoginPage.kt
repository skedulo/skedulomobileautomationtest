package com.skedulo.automation.mobile.pages


import com.skedulo.automation.mobile.helper.LoginHelper
import com.skedulo.automation.mobile.util.OS
import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileBy
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.*
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.PageFactory
import org.openqa.selenium.support.pagefactory.AjaxElementLocator
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory
import kotlin.test.assertFails

class LoginPage(driver: AppiumDriver<*>) : BasePage(driver) {

    @iOSXCUITFindBy(accessibility = "ImageLogo")
    @AndroidFindBy(accessibility = "Skedulo")
    private val title: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Log in with Salesforce")
    @AndroidFindBy(xpath = "//*[@text='Log in with Salesforce']")
    private val btnSignInWithSalesForce: MobileElement? = null

    @iOSXCUITFindBy(accessibility = "Login to xtest")
    @AndroidFindBy(uiAutomator = "new UiSelector().textContains(\"Login to\")")
    private val altBtnSignInWithSalesForce: MobileElement? = null

    @iOSXCUITFindBy(id = "Log in with team name")
    @AndroidFindBy(xpath = "//*[@text='Log in with team name']")
    private val btnSignInWithTeamName: MobileElement? = null

    @iOSXCUITFindBy(id = "More options")
    @AndroidFindBy(xpath = "//*[@text='More options']")
    private val btnMoreOptions: MobileElement? = null

    //Text =  Allow "Skedulo" to access your location even when your are not using the app?
    @iOSXCUITFindBy( accessibility = "Always Allow")
    private val btnAlwaysAllow: MobileElement? = null

    //Text = "Skedulo" would like to Send you notifications
    @iOSXCUITFindBy( accessibility = "Allow")
    private val btnAllow: MobileElement? = null

    //Allow Skedulo to access this device's location?
    @AndroidFindBy( id = "permission_allow_button")
    private val btnAndroidAllow: MobileElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }
    val isPageLoaded: Boolean?
        get() {
            if( os == OS.AWSIOS || os == OS.IOS) {
                try {
                    waitForElement(btnAlwaysAllow, 5)
                    btnAlwaysAllow?.click()
                    waitForElement(btnAllow, 5)
                    btnAllow?.click()
                } catch (e: Exception) {
                }
            }
            if(os == OS.ANDROID){
                try{
                    waitForElement(btnAndroidAllow, 5)
                    btnAndroidAllow?.click()
                } catch (e: Exception) {
                }
            }
            waitForElement(btnMoreOptions, 5)
            var result: Boolean = false
            try{
                if(btnMoreOptions!!.isDisplayed) {
                    result = true
                }
            }catch(e:java.lang.Exception){
                takeScreenshot("LoginPage.isPageloaded :")
            }
            return result
            //waitForElement(title, 5)
            //return title!!.isDisplayed
        }
    val isLoginPageLoaded: Boolean?
    get() {
            var result: Boolean
            try {
                waitForElement(btnMoreOptions)
                btnMoreOptions!!.isDisplayed
                result = true
            }catch(e: Exception){
                result = false
            }
            return result
        }
//    val isSalesForceLoginAvailable: Boolean
//        get() {
//            var result: Boolean
//            try {
//                btnSignInWithSalesForce!!.isDisplayed
//                result = true
//            }catch(e: Exception){
//                result = false
//            }
//            return result
//        }

    fun clickSignInWithSalesForce() {
        try {
            //println("clickSignInWithSalesForce")
            waitForElement(btnSignInWithSalesForce, 5)
            btnSignInWithSalesForce?.click()
        }catch(e: java.lang.Exception){
            //println("Click Sign in with Salesforce but got an exception")
            //altBtnSignInWithSalesForce?.click()
            btnMoreOptions?.click()
            LoginHelper().temporaryFixForLogin()
        }
    }
    fun clickSignInWithUser(){

            altBtnSignInWithSalesForce?.click()

    }

    fun clickSignInWithTeamName() {
        try {
            waitForElement(btnSignInWithTeamName, 5)
            btnSignInWithTeamName!!.click()
        }catch(e: java.lang.Exception){
            btnMoreOptions?.click()
            LoginHelper().temporaryFixForStandaloneLogin()
        }
    }

    fun clickMoreOptions() {

        btnMoreOptions!!.click()
    }

}
