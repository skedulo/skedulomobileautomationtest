package com.skedulo.automation.mobile.tests

import com.skedulo.automation.mobile.pages.HomePage
import com.skedulo.automation.mobile.helper.LoginHelper
import com.skedulo.automation.mobile.tests.AbstractBaseTests.TestBase
import cucumber.api.CucumberOptions
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then

@CucumberOptions(strict = true, monochrome = true, features = ["classpath:LogoutTest","classpath:NavigationTest"], plugin = ["com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:", "json:target/cucumber-report.json"])
open class LoginCommon(): TestBase(){

    @Given("^I login to the app$")
    fun i_login_to_the_app() {
        val loginHelper = LoginHelper()
        loginHelper.login()
    }

    //The below is adding by Jia
    @Then("^I login to the app with different user$")
    fun i_login_to_the_app_with_different_user() {
        val loginHelper = LoginHelper()
        loginHelper.loginWithDifferenUser()
    }


    @Given("^I am logged in to the app$")
    @Throws(Throwable::class)
    fun i_am_logged_in_to_the_app() {
        val homePage = HomePage(getAppiumDriver())
        homePage.isPageLoaded

    }
    @Given("^I login to standalone with the app$")
    fun i_login_to_standalone_with_the_app() {
        val loginHelper = LoginHelper()
        loginHelper.loginStandalone()

    }
}
