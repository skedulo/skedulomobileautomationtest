package com.skedulo.automation.mobile.tests

import com.skedulo.automation.mobile.pages.HomePage
import com.skedulo.automation.mobile.pages.LoginPage
import com.skedulo.automation.mobile.pages.SalesForceLoginPage
import com.skedulo.automation.mobile.tests.AbstractBaseTests.TestBase
import com.skedulo.automation.mobile.util.OS
import cucumber.api.CucumberOptions
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.testng.Assert
import kotlin.test.assertTrue

@CucumberOptions(strict = true, monochrome = true, features = ["classpath:LoginTest"], plugin = ["com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:", "json:test-output/cucumber-report.json"])
open class LoginTest() : TestBase() {
    private var loginPage: LoginPage? = null

    @Given("^I navigate to the login page$")
    fun i_navigate_to_the_login_page() {

        loginPage = LoginPage(getAppiumDriver())
        assertTrue(loginPage?.isPageLoaded!!, "Login page is not loaded !")
    }

    @When("^I choose to login with \"([^\"]*)\"$")
    @Throws(Throwable::class)
    fun i_choose_to_login_with(alternative: String) {
        when(alternative) {
            "salesforce" -> this.loginPage?.clickSignInWithSalesForce()
        }

    }

    @When("^I login with \"([^\"]*)\" and \"([^\"]*)\"$")
    @Throws(Throwable::class)
    fun i_fill_in_email_with(email: String, password: String) {
        val sfLoginPage = SalesForceLoginPage(getAppiumDriver())
        sfLoginPage.isPageLoaded
        sfLoginPage.setEmail(email)
        if(os == OS.AWSANDROID){ getAppiumDriver().hideKeyboard()}
        sfLoginPage.setPassword(password)
        try {
            getAppiumDriver().hideKeyboard()
        } catch(e: Exception) {} //No keyboard was present
        sfLoginPage.clickLogin()

    }

    @Then("^I should be on the welcome page$")
    @Throws(Throwable::class)
    fun i_should_be_on_the_welcome_page() {
        val home = HomePage(getAppiumDriver())
        Assert.assertTrue(home.isPageLoaded!!, "Page is not loaded !")

    }
    @Then("^I should get the \"([^\"]*)\"$")
    @Throws(Throwable::class)
    fun i_should_get_the(result: String) {

        when(result) {
            "pass" -> {
                val home = HomePage(getAppiumDriver())
                Assert.assertTrue(home.isPageLoaded!!, "Page is not loaded !")
            }
            "fail" -> {
                val sfLogin = SalesForceLoginPage(getAppiumDriver())
                Assert.assertTrue(sfLogin.isErrorDisplayed, "Error is not displayed!")
            }

        }

    }

}
