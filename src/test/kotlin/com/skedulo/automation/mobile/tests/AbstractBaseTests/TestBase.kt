/*
 * Copyright 2014-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 * http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.skedulo.automation.mobile.tests.AbstractBaseTests

import com.aventstack.extentreports.ExtentReports
import com.aventstack.extentreports.ExtentTest
import com.aventstack.extentreports.Status
import com.aventstack.extentreports.reporter.ExtentBDDReporter

import com.skedulo.automation.mobile.model.MobileDevice
import com.skedulo.automation.mobile.util.OS
import com.skedulo.automation.mobile.model.Properties
import com.skedulo.automation.mobile.model.TestUser
import com.skedulo.automation.mobile.pages.*
import com.skedulo.automation.mobile.properties.ConfigFileReader
import com.skedulo.automation.mobile.properties.FileReaderManager

import cucumber.api.testng.AbstractTestNGCucumberTests
import cucumber.api.testng.TestNGCucumberRunner
import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.android.AndroidDriver
import io.appium.java_client.ios.IOSDriver
import io.appium.java_client.remote.AndroidMobileCapabilityType
import io.appium.java_client.remote.IOSMobileCapabilityType
import io.appium.java_client.remote.MobileCapabilityType
import org.apache.commons.codec.binary.Base64
import org.apache.commons.io.FileUtils
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.TakesScreenshot
import org.openqa.selenium.OutputType
import org.openqa.selenium.TimeoutException
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import org.testng.ITestContext
import org.testng.ITestResult
import org.testng.Reporter
import org.testng.annotations.*
import java.io.File
import java.io.IOException
import java.net.MalformedURLException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec


/**
 * An abstract base for all of the Android tests within this package
 *
 * Responsible for setting up the Appium test Driver
 */
abstract class TestBase : AbstractTestNGCucumberTests() {
    /**
     * This method runs before any other method.
     *
     * Appium follows a client - server model:
     * We are setting up our appium client in order to connect to Device Farm's appium server.
     *
     * We do not need to and SHOULD NOT set our own DesiredCapabilities
     * Device Farm creates custom settings at the server level. Setting your own DesiredCapabilities
     * will result in unexpected results and failures.
     *
     * @throws MalformedURLException An exception that occurs when the URL is wrong
     */
    @BeforeSuite
    @Throws(MalformedURLException::class)
    fun setUpAppium(context: ITestContext) {
        os = getOsProperty()
        val properties = getProperties()
        var device = getMobileDeviceFromConfig()
        //setScreenShotDir(os)
        println("Device = ${device.deviceName}")
        println("Properties mobile_device_os = $properties.mobile_device_os")
        println("************ Running test on os = $os")
        driver = setupDriver(properties, device)
        driver.let {
            it.manage()?.timeouts()?.implicitlyWait(30, TimeUnit.SECONDS)
        }
        startReporting()
        logger = extent!!.createTest(context.name)
    }

    /**
     * The annotated method will be run after each test method.
     */
    @AfterMethod(alwaysRun = true)
	fun tearDown(result: ITestResult, context: ITestContext)  {
        println("Testresult = $result.getStatus()")
        if(result.isSuccess){
            println("Test success = ${result.getInstanceName()}")
            var execTime = result.endMillis - result.startMillis
            println("Test execution time = $execTime")
            logger!!.pass(context.name)
        }else{
            println("Test failed will take a screenshot with name = ${result.instanceName}")
            takeScreenshots(result.instanceName)
            logger!!.fail(context.name)
        }
	}
    /**
     * The annotated method will be run after all tests in this suite have run.
     *
     * Always remember to quit
     */
    @AfterSuite
    fun tearDownAppium() {
        //println("IN teardownAppium")
        endReport()
        driver.quit()

    }


    /**
     * The annotated method will be run after all the test methods in the current class have been run.
     *
     * Restart the app after every test class to go back to the main
     * screen and to reset the behavior
     */
    @AfterClass
    fun restartApp() {
        try {
            //println("In restartApp")
            val homePage = HomePage(driver)
            if(homePage.isPageLoaded!!) {
                homePage.clickMoreMenu()
                val moreMenu = MorePage(driver)
                moreMenu.isPageLoaded
                moreMenu.clickLogout()
                //println("Confirm logout")
                val confirmLogoutPage = ConfirmLogoutPage(driver)
                confirmLogoutPage.isPageLoaded
                confirmLogoutPage.clickLogout()
                val loginPage = LoginPage(driver)
                loginPage.isLoginPageLoaded
            }
        }catch(e: Exception){
            //takeScreenshot("Exception at logout")
        }
        driver.resetApp()

    }
    private fun setScreenShotDir(os: OS) {
        when(os) {
            OS.AWSANDROID, OS.AWSIOS -> {
                System.setProperty("screenshot.dir",System.getProperty("appium.screenshots.dir" + System.getProperty("java.io.tmpdir", "")))
            }
            OS.ANDROID, OS.IOS -> {
                System.setProperty("screenshot.dir", File(System.getProperty("user.dir")).absolutePath + "/target/surefire-reports")
            }
        }
    }
    private fun getProperties(): Properties {
        return FileReaderManager().jsonReader.getProperties(
            ConfigFileReader().getTestDataResourcePath(),
            ConfigFileReader().getTestDataResourceFileName())
    }
    private fun getOsProperty(): OS {
        var propertyOs: String = System.getProperty("platform") ?: "AWSANDROID"
        var os:OS = when {
            propertyOs.equals("ANDROID", true) -> OS.ANDROID
            propertyOs.equals("IOS", true) -> OS.IOS
            propertyOs.equals("AWSANDROID", true) -> OS.AWSANDROID
            propertyOs.equals("AWSIOS", true) -> OS.AWSIOS
            else -> OS.AWSANDROID
        }
        return os
    }

    private fun takeScreenshots(name: String): Boolean{
        when(os) {
            OS.AWSANDROID, OS.AWSIOS -> {
                val screenshotDirectory = System.getProperty("appium.screenshots.dir", System.getProperty("java.io.tmpdir", ""))
                val screenshot = (driver as TakesScreenshot).getScreenshotAs(OutputType.FILE)
                println("screenshotDirectory = $screenshotDirectory")
                println("file name = $name .png")
                screenshot.renameTo(File(screenshotDirectory, String.format("%s.png", name)))
            }
            OS.ANDROID, OS.IOS -> {

                val calendar = Calendar.getInstance()
                val formatter = SimpleDateFormat("dd_MM_yyyy_hh_mm_ss")

                val scrFile = (driver as TakesScreenshot).getScreenshotAs(OutputType.FILE)
                try {
                    val reportDirectory = File(System.getProperty("user.dir")).absolutePath + "/target/surefire-reports"
                    println("Report directory : $reportDirectory")
                    val destFile = File(reportDirectory + "/failure_screenshots/" + name + "_" + formatter.format(calendar.time) + ".png")
                    println("Destination file = $destFile")
                    FileUtils.copyFile(scrFile, destFile)
                    Reporter.log("<a href='" + destFile.absolutePath + "'> <img src='" + destFile.absolutePath + "' height='100' width='100'/> </a>")
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
        return true
    }

    private fun getMobileDeviceFromConfig():MobileDevice {
        var properties: Properties = getProperties()
        var mobileDevice = System.getProperty("devicename") ?: "SAMSUNG-SM-T337A"
        return properties.mobile_devices.find { it.deviceName == mobileDevice}!!
    }
    public fun getUserFromConfig(user: String): TestUser {
        var properties: Properties = getProperties()
        var testUsers = System.getProperty("user_name") ?: "XTest"
        print("Testuser =  $testUsers")
        return properties.test_users.find { it.user_name == user }!!
    }
    private fun setupDriver(properties: Properties, device: MobileDevice): AppiumDriver<MobileElement> {
        val url = URL("http://127.0.0.1:4723/wd/hub")
        val capabilities = DesiredCapabilities()
        capabilities.setCapability("autoGrantPermissions", true)
        capabilities.setCapability(MobileCapabilityType.TAKES_SCREENSHOT, true)
        when(os) {
            OS.AWSANDROID -> {
                //Aws device farm app name, package name and osversion
                capabilities.setCapability("platformName", "Android")
                capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, properties.mobile_bundle_id)
                capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, properties.mobile_android_app_activity)
                driver = AndroidDriver(url, capabilities)
            }
            OS.ANDROID -> {
                capabilities.setCapability("platformName", device.platformName)
                capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, properties.mobile_bundle_id)
                capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, properties.mobile_android_app_activity)
                capabilities.setCapability("unicodeKeyboard", true)
                capabilities.setCapability("resetKeyboard", true)
                capabilities.setCapability("noReset", false)
                capabilities.setCapability("deviceName", device.deviceName)
                capabilities.setCapability(MobileCapabilityType.UDID, device.udid)
                driver = AndroidDriver(url, capabilities)
            }
            OS.AWSIOS -> {
                capabilities.setCapability("platformName", "iOS")
                capabilities.setCapability("automationName", "XCUITest")
                capabilities.setCapability(IOSMobileCapabilityType.BUNDLE_ID, properties.mobile_bundle_id)
                driver = IOSDriver(url, capabilities)

            }
            OS.IOS -> {
                capabilities.setCapability("platformName", device.platformName)
                capabilities.setCapability("platformVersion", device.platformVersion)
                capabilities.setCapability("automationName", "XCUITest")
                capabilities.setCapability(IOSMobileCapabilityType.BUNDLE_ID, properties.mobile_bundle_id)
                capabilities.setCapability("xcodeOrgId", properties.mobile_ios_xcodeOrgId)
                capabilities.setCapability("xcodeSigningId", properties.mobile_ios_xcodeSigningId)
                //capabilities.setCapability("startIWDP", "true")//Start ios-webkit-debug-proxy(for Webviews)
                capabilities.setCapability(MobileCapabilityType.AUTO_WEBVIEW, true)
                capabilities.setCapability("app", properties.mobile_app_path + "Skedulo.iOS.ipa")
                capabilities.setCapability("deviceName", device.deviceName)
                capabilities.setCapability(MobileCapabilityType.UDID, device.udid)
                capabilities.setCapability("showXcodeLog", true)
                capabilities.setCapability("noReset", true)
                capabilities.setCapability("useNewWDA", false)
                capabilities.setCapability("autoWebview", false);//if the app starts with a webview
                capabilities.setCapability("autoGrantPermissions", false)
                driver = IOSDriver(url, capabilities)
            }
        }
        return driver
    }
    fun waitForElement(mobileElement: MobileElement?, timeout: Long): Boolean {
        var elementFound = false
        val wait = WebDriverWait(driver, timeout)
        try {
            wait.until{ ExpectedConditions.visibilityOf(mobileElement)}
            elementFound = true
        }catch (e: TimeoutException) {}
        return elementFound
    }
    fun startReporting(){
        var reportDirectory = ""
        var propertyDir: String = System.getProperty("logDir") ?: "user.dir"
        reportDirectory = when(os) {
            OS.AWSANDROID, OS.AWSIOS -> {
                File(propertyDir).absolutePath + "/test-output/reports"
                print("Create report in directory ${propertyDir}/test-output/reports").toString()
                //File(System.getProperty("user.dir")).absolutePath + "/target/reports"

            }
            OS.ANDROID, OS.IOS -> {
                File(System.getProperty("user.dir")).absolutePath + "/target/reports"
            }
        }
        val bdd = ExtentBDDReporter(reportDirectory)
        extent = ExtentReports()
        extent!!.attachReporter(bdd)
    }
    fun endReport(){
        extent!!.flush()
        //extent.close()
    }
    companion object {
        init{
            println("Singelton class invoked")

        }
        var extent: ExtentReports? = null
        var logger: ExtentTest? = null
        private lateinit var driver: AppiumDriver<MobileElement>
        lateinit var os: OS
        fun getAppiumDriver() = driver
        val env_login = "skedulo_login"
        val env_pass = "skedulo_password"
        val aesKey = SecretKeySpec("YourFavoritePass".toByteArray(), "AES")

        fun encrypt(valueToEnc: String): String {
            val c = Cipher.getInstance("AES")
            c.init(Cipher.ENCRYPT_MODE, aesKey)
            val encValue = c.doFinal(valueToEnc.toByteArray())
            //println("Encrypted value: ${String(encValue)}")
            return Base64.encodeBase64String(encValue)
        }

        fun decrypt(encryptedValue: String): String {
            val c = Cipher.getInstance("AES")
            c.init(Cipher.DECRYPT_MODE, aesKey)
            val encValue = Base64.decodeBase64(encryptedValue)
            //println("Encrypted value: ${String(encValue)}")
            val decryptedVal = c.doFinal(encValue)
            return String(decryptedVal)

        }
    }
}
