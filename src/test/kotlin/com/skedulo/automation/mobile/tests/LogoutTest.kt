package com.skedulo.automation.mobile.tests

import com.skedulo.automation.mobile.pages.*
import com.skedulo.automation.mobile.tests.AbstractBaseTests.TestBase
import cucumber.api.CucumberOptions
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.testng.Assert

//@CucumberOptions(strict = true, monochrome = true, features = ["classpath:LogoutTest"], plugin = ["com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:", "json:test-output/cucumber-report.json"])
open class LogoutTest(): TestBase(){

    @When("^I click on Logout$")
    fun i_click_on_logout(){
        Thread.sleep(1200)
        var home = HomePage(getAppiumDriver())
        Assert.assertTrue(home.isPageLoaded!!, "Page is not loaded !")
        home.clickMoreMenu()
        val morePage=MorePage(getAppiumDriver())
        Assert.assertTrue(morePage.isPageLoaded,"Page is not loaded !")
        morePage.clickLogout()
        val confirmLogoutPage = ConfirmLogoutPage(getAppiumDriver())
        confirmLogoutPage.isPageLoaded
        confirmLogoutPage.clickLogout()
        try {
            /*val youWereLoggedOutPage = YouWereLoggedOutPage(getAppiumDriver())
            youWereLoggedOutPage.isPageLoaded
            youWereLoggedOutPage.clickOkay()*/
        }catch(e:Exception){}//This means that the 'Your were logged out' message hasn't been displayed}
        val loginPage = LoginPage(getAppiumDriver())
        loginPage.isLoginPageLoaded

    }


    @Then("^I should be logged out$")
    fun i_should_be_logged_out(){
        var result = false
        try {
            val loginPage = LoginPage(getAppiumDriver())
            when {
                loginPage.isPageLoaded!! -> result = true
            }
        }catch(e:Exception){
            val salesForceLoginPage = SalesForceLoginPage(getAppiumDriver())
            when {
                salesForceLoginPage.isPageLoaded!! -> result= true
            }
            salesForceLoginPage.clickCloseButton()
        }
        Assert.assertTrue(result, "I should be logged out")
    }

    @When ("^I go to Edit Profile$")
    fun i_go_to_edit_profile(){
        var home = HomePage(getAppiumDriver())
        Assert.assertTrue(home.isPageLoaded!!, "Page is not loaded !")
        home.clickMoreMenu()
        val morePage=MorePage(getAppiumDriver())
        Assert.assertTrue(morePage.isPageLoaded,"Page is not loaded !")
        morePage.clickResource()

    }

    @Then ("^I see correct resource name$")
    fun i_see_correct_resource_name(){
        val editprofilePage=EditProfilePage(getAppiumDriver())
        Assert.assertTrue(editprofilePage.isPageLoaded!!,"Page is not loaded !")
        Assert.assertEquals(editprofilePage.getResourceName(),"XTest Auto2")
        editprofilePage.clickBack()
        val morePage=MorePage(getAppiumDriver())
        Assert.assertTrue(morePage.isPageLoaded,"Page is not loaded !")
        morePage.clickLogout()
        val confirmLogoutPage = ConfirmLogoutPage(getAppiumDriver())
        confirmLogoutPage.isPageLoaded
        confirmLogoutPage.clickLogout()
        try {
            /*val youWereLoggedOutPage = YouWereLoggedOutPage(getAppiumDriver())
            youWereLoggedOutPage.isPageLoaded
            youWereLoggedOutPage.clickOkay()*/
        }catch(e:Exception){}//This means that the 'Your were logged out' message hasn't been displayed}
        val loginPage = LoginPage(getAppiumDriver())
        loginPage.isLoginPageLoaded
    }


}
