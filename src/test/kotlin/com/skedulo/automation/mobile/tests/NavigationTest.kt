package com.skedulo.automation.mobile.tests

import com.skedulo.automation.mobile.helper.NavigationHelper
import com.skedulo.automation.mobile.tests.AbstractBaseTests.TestBase
import com.skedulo.automation.mobile.pages.*
import com.skedulo.automation.mobile.util.OS
import cucumber.api.CucumberOptions
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.testng.Assert

@CucumberOptions(strict = true, monochrome = true, features = ["classpath:NavigationTest"], plugin = ["com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:", "json:test-output/cucumber-report.json"])
class NavigationTest(): TestBase() {

    private var homePage: HomePage? = null

    @When("^I navigate to the more menu$")
    @Throws(Throwable::class)
    fun i_navigate_to_the_more_menu() {
        homePage = HomePage(getAppiumDriver())
        homePage?.isPageLoaded
        homePage?.clickMoreMenu()
    }
    @When("^I navigate to the agenda menu$")
    @Throws(Throwable::class)
    fun i_navigate_to_the_agenda_menu() {
        var navigationBarComponent = NavigationBarComponent(getAppiumDriver())
        navigationBarComponent.isPageLoaded
        navigationBarComponent!!.clickAgenda()

    }
    @Then("^I should be able to navigate to Settings Unavailability and Shifts page$")
    @Throws(Throwable::class)
    fun i_should_be_able_to_navigate_to_settings_unavailability_and_shifts_page(){
        val navigationHelper = NavigationHelper()
        navigationHelper.navigateAndVerifyMoreMenu()
    }
    @When("^I navigate in the menu$")
    @Throws(Throwable::class)
    fun i_navigate_in_the_menu(){

    }

    @Then("^I should be able to go to Offers Messages and Notifications$")
    @Throws(Throwable::class)
    fun i_should_be_able_to_go_to_Offers_Messages_and_Notifications(){
        val  navigationHelper=NavigationHelper()
        navigationHelper.navigateAndVerifyhNavigationMenu()
    }
    @Then("^I should be able to navigate through the alternatives$")
    @Throws(Throwable::class)
    fun i_should_be_able_to_go_to_navigate_through_the_alternatives() {
        val navigationHelper = NavigationHelper()
        navigationHelper.navigateAndVerifyNavigationMenuAlternative()
    }

    @When("^I navigate to Localisation$")
    @Throws(Throwable::class)
    fun i_navigate_to_localisation(){
        val  navigationHelper=NavigationHelper()
        navigationHelper.openLocalisationFromAgenda()

    }

    @Then("^I should be able to change Localisation setting$")
    @Throws(Throwable::class)
    fun i_should_be_able_to_change_localisation_setting(){
        val localisationPage = LocalisationPage(getAppiumDriver())
        localisationPage.isPageLoaded
        Assert.assertTrue(localisationPage.isTickOnKilometres!!,"The tick is missing")
        Assert.assertTrue(localisationPage.isTickOnDayMonthYear!!,"The tick is missing")
        localisationPage.clickMiles()
        Assert.assertTrue(localisationPage.isTickOnMiles!!,"The tick is missing")
        localisationPage.clickMonthDayYear()
        Assert.assertTrue(localisationPage.isTickOnMonthDayYear!!,"The tick is missing")
        localisationPage.clickKilometres()
        Assert.assertTrue(localisationPage.isTickOnKilometres!!,"The tick is missing")
        localisationPage.clickDayMonthYear()
        Assert.assertTrue(localisationPage.isTickOnDayMonthYear!!,"The tick is missing")

        localisationPage.clickBack()
        val settingsPage=SettingsPage(getAppiumDriver())
        settingsPage.isPageLoaded
        settingsPage.clickBack()
        val moreMenu = MorePage(getAppiumDriver())
        moreMenu.isPageLoaded
        moreMenu.clickBack()
    }
}
