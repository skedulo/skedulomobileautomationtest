package com.skedulo.automation.mobile.tests

import com.skedulo.automation.mobile.pages.HomePage
import com.skedulo.automation.mobile.pages.StandaloneLoginPage
import com.skedulo.automation.mobile.tests.AbstractBaseTests.TestBase
import cucumber.api.CucumberOptions
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.testng.Assert

@CucumberOptions(strict = true, monochrome = true, features = ["classpath:StandaloneTest"], plugin = ["com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:", "json:test-output/cucumber-report.json"])
class LoginStandaloneTest(): TestBase() {


    //When I have logged in
    @When("^When I am logging in$")
    @Throws(Throwable::class)
    fun i_am_logging_in() {
        val standaloneLoginPage = StandaloneLoginPage(getAppiumDriver())
        standaloneLoginPage.isPageLoaded
        val user = getUserFromConfig("btest")
        standaloneLoginPage.setEmail(user.user_email)
        standaloneLoginPage.setPassword(user.user_password)
        try {
            TestBase.getAppiumDriver().hideKeyboard()
        } catch(e: Exception) {} //No keyboard was present
        standaloneLoginPage.clickLogin()


    }
    @Then("^I should be logged in$")
    @Throws(Throwable::class)
    fun i_should_be_logged_in() {
        val home = HomePage(getAppiumDriver())
        Assert.assertTrue(home.isPageLoaded!!, "Page is not loaded !")
    }
}
