package com.skedulo.automation.mobile.api.model

class Regions {
    data class RegionsResponseObject(
        val `data`: Data
    )

    data class Data(
        val regions: Regions
    )

    data class Regions(
        val edges: List<Edge>
    )

    data class Edge(
        val node: Node
    )

    data class Node(
        val UID: String,
        val Name: String
    )
}
