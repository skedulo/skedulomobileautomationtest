package com.skedulo.automation.mobile.api.model

import java.rmi.server.UID

class Jobs {
    data class JobsResponseObject(
        val `data`: Data
    )
    data class Data(
        val jobs: Jobs
    )
    data class Jobs(
        val uID: UID,
        val name: String,
        val description: String,
        val jobStatus: String,
        val geoLatitude: Double,
        val geoLongitude: Double
    )
}
