package com.skedulo.automation.mobile.api.model.mobile.availability

data class AvailabilityResponseObject(
    val result: Result
)
data class AdditionalProp(
        val Finish: String,
        val IsAvailable: Boolean,
        val Reason: String,
        val Start: String
)
data class Result(
        val additionalProp: List<AdditionalProp>
)
