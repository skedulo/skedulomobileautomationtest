package com.skedulo.automation.mobile.api
import com.skedulo.automation.mobile.api.annotations.Authorization
import com.skedulo.automation.mobile.api.annotations.AuthorizationType
import com.skedulo.automation.mobile.api.annotations.BaseUrl
import com.skedulo.automation.mobile.api.model.Jobs
import com.skedulo.automation.mobile.api.model.Regions
import com.skedulo.automation.mobile.properties.ConfigFileReader
import okhttp3.Call
import okhttp3.RequestBody
import retrofit2.http.*

@Authorization(AuthorizationType.BEARERTOKEN)
@BaseUrl("https://dev-api.test.skl.io")
interface SkeduloGraphQLApi {


    @POST("graphql/graphql")
    fun getRegions(
        @Body data: RequestBody
    ): retrofit2.Call<Regions.RegionsResponseObject>

    @POST("graphql/graphql")
    fun getQueuedJobs(
        @Body data: RequestBody
    ): retrofit2.Call<Jobs.JobsResponseObject>

}

//    @GET("graphql/graphql")
//    fun getData(
//        @Body getJobsGraphQLQuery: getJobsGraphQLQuery
//    ) : Call<GetJobOffers.QueryResponseObject> {
//    }

//}
