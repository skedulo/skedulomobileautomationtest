package com.skedulo.automation.mobile.util

import com.skedulo.automation.mobile.tests.AbstractBaseTests.TestBase
import java.io.*
import java.util.*
import java.text.*
import org.apache.commons.io.FileUtils

import org.openqa.selenium.*

import org.testng.*

class ScreenshotListener : ITestListener {
    override fun onTestFailedButWithinSuccessPercentage(p0: ITestResult?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onTestStart(p0: ITestResult?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onStart(p0: ITestContext?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onTestFailure(result: ITestResult) {
        println("onTestFailure result = $result.getStatus()")
        val calendar = Calendar.getInstance()
        val formater = SimpleDateFormat("dd_MM_yyyy_hh_mm_ss")
        val methodName = result.instanceName
        if (!result.isSuccess) {
            val scrFile = (TestBase.Companion.getAppiumDriver() as TakesScreenshot).getScreenshotAs(OutputType.FILE)
            try {
                val reportDirectory = File(System.getProperty("user.dir")).absolutePath + "/target/surefire-reports"
                val destFile = File(reportDirectory + "/failure_screenshots/" + methodName + "_" + formater.format(calendar.time) + ".png")
                FileUtils.copyFile(scrFile, destFile)
                Reporter.log("<a href='" + destFile.absolutePath + "'> <img src='" + destFile.absolutePath + "' height='100' width='100'/> </a>")
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }

    override fun onFinish(p0: ITestContext?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onTestSkipped(p0: ITestResult?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onTestSuccess(p0: ITestResult?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
