package com.skedulo.automation.mobile.util

enum class OS {
    ANDROID, IOS, AWSANDROID, AWSIOS
}