package com.skedulo.automation.mobile.util

import com.fasterxml.jackson.databind.ObjectMapper
import cucumber.api.TypeRegistry
import cucumber.api.TypeRegistryConfigurer
import io.cucumber.cucumberexpressions.ParameterByTypeTransformer
import io.cucumber.datatable.TableCellByTypeTransformer
import io.cucumber.datatable.TableEntryByTypeTransformer
import java.lang.reflect.Type
import java.util.*
import java.util.Locale.ENGLISH

class TypeRegistryConfiguration : TypeRegistryConfigurer {

    override fun locale(): Locale {
        return ENGLISH
    }

    override fun configureTypeRegistry(typeRegistry: TypeRegistry) {
////        var transformer = Transformer()
//        typeRegistry.setDefaultDataTableCellTransformer(transformer)
//        typeRegistry.setDefaultDataTableEntryTransformer(transformer)
//        typeRegistry.setDefaultParameterTransformer(transformer)
    }

//    class Transformer : ParameterByTypeTransformer, TableEntryByTypeTransformer, TableCellByTypeTransformer {
//        val objectMapper: ObjectMapper = ObjectMapper()
//
//        override fun transform(s: String, type: Type) {
//            return objectMapper.convertValue(s, objectMapper.constructType(type))
//        }
//
//        override fun transform(map: Map<String, String>, aClass: Class<T> , tableCellByTypeTransformer: TableCellByTypeTransformer): T  {
//            return objectMapper.convertValue(map, aClass)
//        }
//
//        override fun transform(s: String, aClass: Class<T>): T {
//            return objectMapper.convertValue(s, aClass)
//        }
//    }
}
