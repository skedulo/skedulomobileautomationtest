package com.skedulo.automation.mobile.properties

import java.io.File
import java.io.FileReader
import java.util.*


class ConfigFileReader {
    private val properties = Properties()
    private val propertiesFile = System.getProperty("user.dir") + "/src/test/resources/testDataResources/Configuration.properties";
    private val serverPropertiesFile =  "Configuration.properties";

    init{
        try {
            val reader = FileReader(propertiesFile)
            properties.load(reader)
        }catch(e:java.io.FileNotFoundException){
            val reader = FileReader(serverPropertiesFile)
            properties.load(reader)
        }
        //properties.forEach { (k, v) -> println("key = $k, value = $v") }
    }
    fun getTestDataResourcePath(): String {
        return properties.getProperty("testDataResourcePath")
    }
    fun getTestDataResourceFileName(): String {
        return properties.getProperty("testDataResourceFile")
    }
    fun getTestDataApiUser(): String {
        return properties.getProperty("testApiUser")
    }
    fun getTestDataApiPassword(): String {
        return properties.getProperty("testApiPassword")
    }
    fun getTestDataApiToken(): String {
        return properties.getProperty("testApiToken")
    }
    fun getTestDataApiGetRegionQuery(): String {
        return properties.getProperty("testApiGetRegionsQuery")
    }
    fun getTestDataApiGetQueuedJobsQuery(): String {
        return properties.getProperty("testApiGetQueuedJobsQuery")
    }
    fun getTestDataApiBaseUrl(): String {
        return properties.getProperty("testApiBaseUrl")
    }
}
