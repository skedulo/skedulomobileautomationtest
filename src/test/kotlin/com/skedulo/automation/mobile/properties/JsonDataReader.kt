package com.skedulo.automation.mobile.properties

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import com.skedulo.automation.mobile.model.MobileDevice
import com.skedulo.automation.mobile.model.Properties
import org.testng.FileAssert
import java.io.File
import java.io.FileReader

class JsonDataReader {
    private var properties: Properties? = null
    fun getProperties(configurationPath: String, configurationFile: String): Properties {

        val mapper = ObjectMapper().registerModule(KotlinModule())
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        var jsonString = try {
            File("$configurationPath$configurationFile").readText(Charsets.UTF_8)
        }catch(e:java.io.FileNotFoundException){
            File("$configurationFile").readText(Charsets.UTF_8)
        }
        properties = jsonString.let { mapper.readValue<Properties>(it) }
        return properties as Properties
    }
}
