package com.skedulo.automation.mobile.properties

class FileReaderManager() {

    val configReader: ConfigFileReader
        get() = configFileReader ?: ConfigFileReader()

    val jsonReader: JsonDataReader
        get() = jsonDataReader ?: JsonDataReader()

    companion object {

        val instance = FileReaderManager()
        private val configFileReader: ConfigFileReader? = null
        private val jsonDataReader: JsonDataReader? = null
    }
}
