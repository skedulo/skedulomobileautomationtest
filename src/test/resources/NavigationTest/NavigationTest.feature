
Feature: Navigation

  Scenario: Navigation

  As a user I should be able to navigate in the menu

    Given I login to the app
    When I navigate in the menu
    Then I should be able to go to Offers Messages and Notifications

    Scenario: Navigation in the menu

    As a user I should be able to navigate in the menu directly

        Given I am logged in to the app
        When I navigate to the agenda menu
        Then I should be able to navigate through the alternatives

    Scenario: More menu navigation

  As a user I should be able to navigate the more menu

    Given I am logged in to the app
    When I navigate to the more menu
    Then I should be able to navigate to Settings Unavailability and Shifts page


  Scenario: Localisation

  As a user I should be able to navigate Localisation page and change Localisation setting

    Given I am logged in to the app
    When I navigate to Localisation
    Then I should be able to change Localisation setting

