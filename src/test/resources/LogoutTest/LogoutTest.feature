Feature: Logout


  Scenario: Logout flow via app

  As a user I should be able to Logout

    Given I login to the app
    When I click on Logout
    Then I should be logged out

  Scenario: Logout then Login with different user

  As a user I should be able to Logout then Login with different user

    Given I login to the app
    When I click on Logout
    Then I login to the app with different user
    When I go to Edit Profile
    Then I see correct resource name